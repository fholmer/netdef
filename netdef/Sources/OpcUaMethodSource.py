import logging

from opcua import uamethod, ua

from ..Interfaces import DefaultInterface
from ..Interfaces.datamessage import DataDefinition
from . import BaseSource, Sources

supported_types = {
    "boolean": "Boolean",
    "sbyte": "SByte",
    "byte": "Byte",
    "int16": "Int16",
    "uint16": "UInt16",
    "int32": "Int32",
    "uint32": "UInt32",
    "int64": "Int64",
    "uint64": "UInt64",
    "float": "Float",
    "double": "Double",
    "string": "String",
    "bytestring": "ByteString",
}


def Arg(name, description, datatype):
    # test if varianttype is supported
    _varianttype_name = datatype.lower().strip()
    if not _varianttype_name in supported_types.keys():
        raise KeyError("{} not supported", datatype)

    # test if varianttype is valid. raises error if not
    objectid = getattr(ua.ObjectIds, supported_types[_varianttype_name])

    arg = ua.Argument()
    arg.Name = name
    arg.DataType = ua.NodeId(objectid)
    arg.ValueRank = -1
    arg.ArrayDimensions = []
    arg.Description = ua.LocalizedText(description)
    return arg


@Sources.register("OpcUaMethodSource")
class OpcUaMethodSource(BaseSource.BaseSource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.interface = DefaultInterface.DefaultInterface
        self._nodeid = self.key

        if DataDefinition.is_uri(self.key):
            # test if nodeid is valid. raises error if not
            datadef = DataDefinition.from_uri(self.key)
            self._nodeid = ua.NodeId.from_string(datadef.key).to_string()
            inputs = datadef.extension.get("inputs", [])
            self.inputs = list([Arg(*args) for args in inputs])
            self.outputs = []

    @uamethod
    def func(self, parent, *args):
        self._set_set_value(args, None, True, "opcua-method")

    def is_method(self):
        return True

    def get_method(self):
        return self.func, self.inputs, self.outputs

    def get_nodeid(self):
        "Returns the nodeid from the source"
        return self._nodeid

    @property
    def value_as_string(self):
        """
        This function is used by webadmin to visualize the value as string
        Bytedata is truncated to limit the size of the string
        """
        if self.value and isinstance(self.value, bytes):
            n = len(self.value)
            return "<{}...><data len:{}>".format(self.value[:10], n)
        else:
            return super().value_as_string
